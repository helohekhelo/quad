#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <stb_image.h>

struct mesh {
	unsigned int vao, vbo, ebo;
};

struct tex_conf {
	int fmt, filter;
	int wraps, wrapt;
};

struct mesh quad_mesh;
unsigned int quad_prog;
int col_loc;
unsigned int tex;

char *read_file(const char *file)
{
	FILE *fp;
	char *buf;
	long len;
	
	fp = fopen(file, "rb");
	if (fp == NULL) {
		fprintf(stderr, "%s: %s\n", file, strerror(errno));
		return NULL;
	}
	
	fseek(fp, 0, SEEK_END);
	len = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	buf = malloc(len + 1);
	if (buf == NULL) {
		fprintf(stderr, "%s: malloc fail\n", file);
		return NULL;
	}
	buf[len] = '\0';

	fread(buf, len, 1, fp);
	fclose(fp);
	return buf;
}

bool comp_shader(const char *file, int type, unsigned int *id)
{
	char *src;
	int ok;

	src = read_file(file);
	if (src == NULL)
		return false;

	*id = glCreateShader(type);
	glShaderSource(*id, 1, (const char **)&src, NULL);
	glCompileShader(*id);
	
	glGetShaderiv(*id, GL_COMPILE_STATUS, &ok);
	if (!ok) {
		int len;
		char *log;

		glGetShaderiv(*id, GL_INFO_LOG_LENGTH, &len);
		log = malloc(len);
		if (log == NULL) {
			fprintf(stderr, "%s: log malloc fail\n", file);
		} else {
			glGetShaderInfoLog(*id, len, NULL, log);
			fprintf(stderr, "%s: %s\n", file, log);
			free(log);
		}
	}

	return ok;
}

bool comp_prog(const char *vert, const char *frag, unsigned int *id)
{
	unsigned int vid, fid;
	int ok;

	comp_shader(vert, GL_VERTEX_SHADER, &vid);
	comp_shader(frag, GL_FRAGMENT_SHADER, &fid);

	*id = glCreateProgram();
	glAttachShader(*id, vid);
	glAttachShader(*id, fid);
	glLinkProgram(*id);

	glGetProgramiv(*id, GL_LINK_STATUS, &ok);	
	if (!ok) {
		int len;
		char *log;

		glGetProgramiv(*id, GL_INFO_LOG_LENGTH, &len);
		log = malloc(len);
		if (log == NULL) {
			fprintf(stderr, "%s: log malloc fail\n", vert);
		} else {
			glGetProgramInfoLog(*id, len, NULL, log);
			fprintf(stderr, "%s: %s\n", vert, log);
			free(log);
		}
	}

	return ok;
}

int ch_to_fmt(int ch)
{
	switch (ch) {
	case 1:
		return GL_RED;
	case 2:
		return GL_RG;
	case 3:
		return GL_RGB;
	case 4:
	default:
		return GL_RGBA;
	}
}

bool load_tex(const char *img, struct tex_conf *conf, unsigned int *id)
{
	int w, h, ch;
	int dfmt;
	unsigned char *data;

	data = stbi_load(img, &w, &h, &ch, 0);
	if (data == NULL)
		return false;

	dfmt = ch_to_fmt(ch);
	glGenTextures(1, id);
	glBindTexture(GL_TEXTURE_2D, *id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, conf->filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, conf->filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, conf->wraps);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, conf->wrapt);

	glTexImage2D(GL_TEXTURE_2D,
			0,
			conf->fmt,
			w,
			h,
			0,
			dfmt,
			GL_UNSIGNED_BYTE,
			data);

	stbi_image_free(data);
	return true;
}

struct mesh gen_quad_mesh(void)
{
	unsigned int vao, vbo, ebo;
	const float data[] = {
		1, 1, 1, 0,
		1, 0, 1, 1,
		0, 1, 0, 0,
		0, 0, 0, 1,
	};
	const unsigned int index[] = {
		0, 1, 2,
		3, 1, 2,
	};
	const size_t stride = sizeof(data) / 4;

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); // pos
	glVertexAttribPointer(0, 2, GL_FLOAT, false, stride, (void *)0);
	glEnableVertexAttribArray(1); // tex
	glVertexAttribPointer(1, 2, GL_FLOAT, false, stride, (void *)(sizeof(float) * 2));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index), index, GL_STATIC_DRAW);

	return (struct mesh){vao, vbo, ebo};
}

void fb_size_cb(GLFWwindow *window, int width, int height)
{
	(void)window;
	glViewport(0, 0, width, height);
}

GLFWwindow *create_window(int width, int height, const char *title)
{
	GLFWwindow *window;

	if (!glfwInit()) {
		fputs("glfw init fail\n", stderr);
		return NULL;
	}

	glfwDefaultWindowHints();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_VISIBLE, false);

	window = glfwCreateWindow(width, height, title, NULL, NULL);
	glfwMakeContextCurrent(window);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		fputs("opengl init fail\n", stderr);
		return NULL;
	}

	glfwSetFramebufferSizeCallback(window, fb_size_cb);

	glfwSwapInterval(1);
	return window;
}

void draw(void)
{
	glUseProgram(quad_prog);
	glUniform3f(col_loc, 1, 1, 1);


	glBindVertexArray(quad_mesh.vao);
	glBindTexture(GL_TEXTURE_2D, tex);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
}

void gl_err(const char *msg)
{
	int err;

	while ((err = glGetError()) != GL_NO_ERROR)
		fprintf(stderr, "gl: %s: %d\n", msg, err);
}

int main(void)
{
	GLFWwindow *window;
	struct tex_conf conf;
	float last, now, dt;
	float fpsWait = 0.f;
	int frames = 0;

	window = create_window(800, 600, "stuff");
	if (window == NULL)
		return 1;

	if (!comp_prog("quadv.glsl", "quadf.glsl", &quad_prog))
		return 1;

	conf.filter = GL_LINEAR;
	conf.fmt = GL_RGB;
	conf.wraps = GL_REPEAT;
	conf.wrapt = GL_REPEAT;

	if (!load_tex("poonis.png", &conf, &tex))
		return 1;

	col_loc = glGetUniformLocation(quad_prog, "col");
	quad_mesh = gen_quad_mesh();
	glClearColor(0, 0, 0, 1);

	last = glfwGetTime();
	glfwShowWindow(window);
	while (!glfwWindowShouldClose(window)) {
		now = glfwGetTime();
		dt = now - last;
		last = now;

		fpsWait += dt;
		if (fpsWait >= 1) {
			float fps = frames / fpsWait;

			frames = 0;
			fpsWait = 0;
			printf("fps: %.3f\n", fps);
		}
		++frames;

		glClear(GL_COLOR_BUFFER_BIT);

		draw();
		
		gl_err("err");
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
}

/*
 * Copyright (c) 2024 helohekhelo
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
